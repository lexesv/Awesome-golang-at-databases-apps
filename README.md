# Databases

### TiDB is a distributed NewSQL database compatible with MySQL protocol 
https://pingcap.com

https://github.com/pingcap/tidb


### CockroachDB - the open source, cloud-native SQL database. 
PostgreSQL wire protocol

https://www.cockroachlabs.com

https://github.com/cockroachdb/cockroach


### The lightweight, distributed relational database built on SQLite. 
http://www.rqlite.com/

https://github.com/rqlite/rqlite

### Package ql is a pure Go embedded SQL database.
https://github.com/cznic/ql




# ORM

### Simple and Powerful ORM for Go, support mysql,postgres,tidb,sqlite3,mssql,oracle 
http://xorm.io

https://github.com/go-xorm/xorm

### Beego ORM - A powerful orm framework for go.
https://github.com/astaxie/beego/tree/master/orm


# Web

### Web-based MySQL database browser. 
Based on pgweb https://github.com/sosedoff/pgweb

https://github.com/smurfpandey/mysqlweb


### Cross-platform client for PostgreSQL databases 
http://sosedoff.github.io/pgweb

https://github.com/sosedoff/pgweb

### Web-Based SQLite database browser
https://github.com/hypebeast/sqliteweb


# Utilites

### USQL is a universal command-line interface for SQL databases
https://github.com/xo/usql

